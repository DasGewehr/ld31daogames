--[[
    ZONE CALLBACKS
--]]

--[[

Init zone callback is invoked once when this zone is created.

--]]

function init(zone)
end

--[[
    HELPER FUNCTIONS
--]]

local function getmsgcode(msg)
    local code = msg:sub(1,2)
    local data = msg:sub(3)
    return code, data
end

local function lastmsgid()
    local id = get('lastmsgid')
    if not id then
        return 0
    else
        return id
    end
end

local function set_lastmsgid(id)
    set('lastmsgid', id)
end

local function msgkey(id)
    if type(id) == 'number' then
        id = tostring(id)
    end
    return 'message.' .. id
end

local function panelkey(id)
  return 'panel' .. id
end

local function split_string(delimiter, msg)
    local elements = {}
    for el in string.gmatch(msg, '([^'..delimiter..']+)') do
        table.insert(elements, el)
    end
    return elements
end

local function join_string(delimiter, list)
    local len = #list
    if len == 0 then
        return ''
    end
    local string = list[1]
    if type(string) == 'number' then
        string = tostring(string)
    end
    for i=2, len do
        local el = list[i]
        if type(el) == 'number' then
            el = tostring(el)
        end
        string = string .. delimiter .. el
    end
    return string
end

--[[
    LOGIC FUNCTIONS
--]]

local function panel_add_msg(panel_id, msg_id)
    local panel = get(panelkey(panel_id))
    if not panel then
        panel = {}
    end
    local key_msg_id = tostring(msg_id)
    panel[key_msg_id] = true
    set(panelkey(panel_id), panel)
end

local function panel_remove_msg(panel_id, msg_id)
    local panel = get(panelkey(panel_id))
    if not panel then
        panel = {}
    end
    local key_msgid = tostring(msg_id)
    panel[key_msgid] = nil
    set(panelkey(panel_id), panel)
end


local function msg_store(player, data)
    local splitted= split_string(':', data)
    local len = #splitted
    if len ~= 2 then
        return '30'
    end
    local m = {}
    m['content'] = splitted[2]
    m['likes'] = 0
    m['dislikes'] = 0
    m['panel_id'] = splitted[1]
    local msgid = lastmsgid() + 1
    set(msgkey(msgid), m)
    set_lastmsgid(msgid)
    panel_add_msg(splitted[1], msgid)
    local resp = '20' .. tostring(msgid)
    return resp
end

local function msg_read(player, msg_id)
    local m = get(msgkey(msg_id))
    if not m then
        return '31'
    end
    local resp = '21' .. join_string(':', {m['panel_id'], m['likes'], m['dislikes'], m['content']})
    return resp
end

local function msg_update(player, data)
    local splitted = split_string(':', data)
    if #splitted ~= 2 then
        return '32'
    end
    local m = get(msgkey(splitted[1]))
    if not m then
        return '32'
    end
    m['content'] = splitted[2]
    set(msgkey(splitted[1]), m)
    return '22'
end

local function msg_delete(player, msg_id)
    local m = get(msgkey(msg_id))
    if not m then
        return '33'
    end
    set(msgkey(msg_id), nil)
    panel_remove_msg(m['panel_id'], msg_id)
    return '23'
end

local function msg_list(player, panel_id)
    local p = get(panelkey(panel_id))
    if not p then
        return '34'
    end
    local mstr_list = {}
    for k,v in pairs(p) do
        if k then
            local msg_id = k
            local m = get(msgkey(msg_id))
            if m then
                local mstr = join_string(':', {msg_id, m['likes'], m['dislikes'], m['content']})
                table.insert(mstr_list, mstr)
            end
        end
    end
    local resp = '24' .. join_string(';', mstr_list)
    return resp
end

local function msg_like(player, msg_id)
    local m = get(msgkey(msg_id))
    if not m then
        return '35'
    end
    m['likes'] = m['likes'] + 1
    set(msgkey(msg_id), m)
    return '25' .. tostring(m['likes'])
end

local function msg_dislike(player, msg_id)
    local m = get(msgkey(msg_id))
    if not m then
        return '36'
    end
    m['dislikes'] = m['dislikes'] + 1
    set(msgkey(msg_id), m)
    return '26' .. tostring(m['likes'])
end

--[[

Handle message is invoked everytime a message is sent from anyone to this zone.

--]]

function handle_message(zone, player, msg)
    local code, data  = getmsgcode(msg)
    if '10' == code then
        local resp = msg_store(player, data)
        send_msg(zone, player, resp)
    elseif '11' == code then
        local resp = msg_read(player, data)
        send_msg(zone, player, resp)
    elseif '12' == code then
        local resp = msg_update(player, data)
        send_msg(zone, player, resp)
    elseif '13' == code then
        local resp = msg_delete(player, data)
        send_msg(zone, player, resp)
    elseif '14' == code then
        local resp = msg_list(player, data)
        send_msg(zone, player, resp)
    elseif '15' == code then
        local resp = msg_like(player, data)
        send_msg(zone, player, resp)
    elseif '16' == code then
        local resp = msg_dislike(player, data)
        send_msg(zone, player, resp)
    end
end

--[[

Player joining is invoked everytime a player joins the zone.

--]]

function player_joining(zone, player)
end

--[[

Player leaving is invoked everytime a player leaves the zone.

--]]

function player_leaving(zone, player)
end

